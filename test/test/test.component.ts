import { Component, OnInit } from '@angular/core';

@Component({
  selector: '[app-test]',
  templateUrl: './test.component.html',
  styles: [`
  
.text-s{
    color: green;
}

.text-d{
  color: red;
}

`]
})
export class TestComponent implements OnInit {

  public name = "Custom Component";
  public myId = 'myIdTest';
  public isDisable = true;
  public isDanger = false;

  public sucClass = "text-s";

  public classArray = {
    "text-s": this.isDanger,
    "text-d": !this.isDanger
  }

  constructor() { }

  ngOnInit() {
  }

}
